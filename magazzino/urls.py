from django.shortcuts import redirect
from django.urls import path
from magazzino import views

app_name = 'magazzino'

urlpatterns = [
    path('', lambda request: redirect('home/', permanent=False)),
    path('home/', views.home, name='home'),
    path('login/', views.LoginUser.as_view(), name='login'),
    path('logout/', views.logout_user, name='logout'),

    path('prodotti/', views.ProdottiList.as_view(), name='prodotti'),
    path('prodotto/create', views.prodotto_create, name='nuovo-prodotto'),
]
