from django.contrib.auth.models import User
from django.forms import forms, ModelForm, Form, DateInput

from magazzino.models import Acquisto, Vendita, Prodotto


class LoginForm(Form):
    class Meta:
        model = User
        fields = ('username', 'password')


class DateInput(DateInput):
    input_type = 'date'


class AcquistoForm(ModelForm):
    class Meta:
        model = Acquisto
        fields = ['Fornitore', 'Prodotto', 'Quantita']


class VenditaForm(ModelForm):
    class Meta:
        model = Vendita
        fields = ['Prodotto', 'Quantita']


class ProdottoForm(ModelForm):
    class Meta:
        model = Prodotto
        fields = ['Nome', 'UnitaDiMisura']
