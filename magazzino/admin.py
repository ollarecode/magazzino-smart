from django.contrib import admin
from magazzino.models import Prodotto, Negozio, Categoria, Fornitore, Deposito, Acquisto, Vendita, Appartiene

admin.site.register(Negozio)
admin.site.register(Prodotto)
admin.site.register(Categoria)
admin.site.register(Fornitore)
admin.site.register(Deposito)
admin.site.register(Acquisto)
admin.site.register(Vendita)
admin.site.register(Appartiene)
