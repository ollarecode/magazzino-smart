from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import ListView

from magazzino import forms
from magazzino.forms import AcquistoForm, VenditaForm, ProdottoForm
from magazzino.models import Negozio, Deposito, Prodotto, Categoria


class LoginUser(LoginView):
    template_name = 'login.html'
    from_class = forms.LoginForm


@login_required
def logout_user(request):
    logout(request)
    return redirect('magazzino:home')


def home(request):
    data = {
        'acquistoFrom': AcquistoForm(),
        'venditaFrom': VenditaForm(),
        'categorie': Categoria.objects.all()
    }
    if request.user.is_authenticated:
        tempNegozio = Negozio.objects.filter(User=request.user).first()
        if tempNegozio is not None:
            data['Negozio'] = tempNegozio.id
        else:
            return HttpResponse('Unauthorized', status=401)

    return render(request, 'magazzino/index.html', data)


class ProdottiList(ListView):
    model = Deposito
    template_name = 'magazzino/prodotti.html'

    def get_queryset(self):
        filter_val = Negozio.objects.filter(User=self.request.user.id).first().id
        new_context = Deposito.objects.filter(
            Negozio_id=filter_val,
        )
        return new_context

    def get_context_data(self,**kwargs):
        context = super(ProdottiList,self).get_context_data(**kwargs)
        context['Negozio'] = Negozio.objects.filter(User=self.request.user).first().id
        return context


@login_required
def prodotto_create(request):
    template_name = 'magazzino/prodotto-create.html'
    data = {
        'form': ProdottoForm()
    }

    return render(request, 'magazzino/prodotto-create.html', data)

