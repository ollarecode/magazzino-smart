from django.contrib import auth
from django.db import models
from django.db.models import CASCADE

from magazzino.Service import DepositoService

'''
Per motivi di comodità non ho assegnato le chiavi primarie corrette ma ho utilizzato su ogni tabella il campo 'id' 
che viene inizializzato in automatico da django.

Questo sito è stato pensato per aiutare a supervisionare i prodotti del tuo negozio, trovare i prodotti di tendenza e 
semplificare la gestione del magazzino. Non è stato pensato per gestire la parte contabile del magazzino ma solo quella
logica per poter fare in base ai grafici degli interventi sull'efficienza dell'attività.
'''


class Negozio(models.Model):
    Nome = models.CharField(max_length=64)
    User = models.ForeignKey(auth.get_user_model(), on_delete=CASCADE)

    def __str__(self):
        return self.Nome

    class Meta:
        verbose_name_plural = 'Negozi'


class Prodotto(models.Model):
    Nome = models.CharField(max_length=64)
    UnitaDiMisura = models.CharField(max_length=16)

    def __str__(self):
        return self.Nome

    class Meta:
        verbose_name_plural = 'Prodotti'


class Categoria(models.Model):
    Nome = models.CharField(max_length=64)
    Descrizione = models.TextField(max_length=1024)

    def __str__(self):
        return self.Nome

    class Meta:
        verbose_name_plural = 'Categorie'


class Fornitore(models.Model):
    Nome = models.CharField(max_length=64)
    Descrizione = models.TextField(max_length=1024)

    def __str__(self):
        return self.Nome

    class Meta:
        verbose_name_plural = 'Fornitori'


class Deposito(models.Model):
    Negozio = models.ForeignKey(Negozio, on_delete=CASCADE)
    Prodotto = models.ForeignKey(Prodotto, on_delete=CASCADE)
    Quantita = models.FloatField()
    Descrizione = models.TextField(max_length=1024)

    def __str__(self):
        return self.Prodotto.Nome + ' ' + str(self.Quantita)

    class Meta:
        verbose_name_plural = 'Deposito (Prodotto -> Negozio)'


class Acquisto(models.Model):
    Negozio = models.ForeignKey(Negozio, on_delete=CASCADE)
    Fornitore = models.ForeignKey(Fornitore, on_delete=CASCADE, null=True, blank=True)
    Prodotto = models.ForeignKey(Prodotto, on_delete=CASCADE)
    Quantita = models.FloatField()
    DataA = models.DateTimeField()

    # Programmatically implemented SQL triggers
    previus_quantity = 0

    def __init__(self, *args, **kwargs):
        super(Acquisto, self).__init__(*args, **kwargs)

        # discrimino update e insert usando gli argomenti
        if kwargs.get('Quantita', False):
            self.previus_quantity = 0
        else:
            self.previus_quantity = self.Quantita

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self.previus_quantity is None:
            self.previus_quantity = 0
        delta_quantity = self.previus_quantity - self.Quantita
        DepositoService.modify_deposit_product_supply(self.Prodotto, self.Negozio, delta_quantity * -1)

    def delete(self, *args, **kwargs):
        super().delete(*args, **kwargs)
        DepositoService.modify_deposit_product_supply(self.Prodotto, self.Negozio, self.Quantita * -1)

    def __str__(self):
        return self.Negozio.Nome + ' / ' + self.Fornitore.Nome + ' -> ' + str(self.Quantita) + str(self.Prodotto.UnitaDiMisura) + ' ' + self.Prodotto.Nome #sas

    class Meta:
        verbose_name_plural = 'Acquisti'


class Vendita(models.Model):
    Negozio = models.ForeignKey(Negozio, on_delete=CASCADE)
    Prodotto = models.ForeignKey(Prodotto, on_delete=CASCADE)
    Quantita = models.FloatField()
    DataV = models.DateTimeField()

    # Programmatically implemented SQL triggers
    previus_quantity = 0

    def __init__(self, *args, **kwargs):
        super(Vendita, self).__init__(*args, **kwargs)

        # discrimino update e insert usando gli argomenti
        if kwargs.get('Quantita', False):
            self.previus_quantity = 0
        else:
            self.previus_quantity = self.Quantita

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self.previus_quantity is None:
            self.previus_quantity = 0
        delta_quantity = self.previus_quantity - self.Quantita
        DepositoService.modify_deposit_product_supply(self.Prodotto, self.Negozio, delta_quantity)
        self.previus_quantity = self.Quantita

    def delete(self, *args, **kwargs):
        super().delete(*args, **kwargs)
        DepositoService.modify_deposit_product_supply(self.Prodotto, self.Negozio, self.Quantita)

    def __str__(self):
        return self.Negozio.Nome + ' -> ' + str(self.Quantita) + str(self.Prodotto.UnitaDiMisura) + ' ' + self.Prodotto.Nome

    class Meta:
        verbose_name_plural = 'Vendite'


class Appartiene(models.Model):
    Negozio = models.ForeignKey(Negozio, on_delete=CASCADE)
    Prodotto = models.ForeignKey(Prodotto, on_delete=CASCADE)
    Categoria = models.ForeignKey(Categoria, on_delete=CASCADE)

    def __str__(self):
        return self.Prodotto.Nome + ' -> ' + self.Categoria.Nome

    class Meta:
        verbose_name_plural = 'Appartiene (Prodotto -> Categoria)'
