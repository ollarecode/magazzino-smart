import magazzino


def modify_deposit_product_supply(prodotto=None, negozio=None, supply_delta=None):
    # Checking if has been called with id or object references
    if not isinstance(prodotto, magazzino.models.Prodotto):
        prodotto = magazzino.models.Prodotto.objects.get(pk=prodotto)

    if not isinstance(negozio, magazzino.models.Negozio):
        negozio = magazzino.models.Negozio.objects.get(pk=negozio)

    if not isinstance(supply_delta, float) and not isinstance(supply_delta, int):
        supply_delta = float(supply_delta)

    deposito = magazzino.models.Deposito.objects.filter(Negozio=negozio, Prodotto=prodotto)
    if not deposito:
        magazzino.models.Deposito.objects.create(Negozio=negozio, Prodotto=prodotto, Quantita=supply_delta).save()
    else:
        deposito = deposito.get()
        deposito.Quantita = deposito.Quantita + supply_delta
        deposito.save()
