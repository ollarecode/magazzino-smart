from django.db.models import F, Count
from rest_framework import generics, status
from rest_framework.decorators import api_view
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from api.Contracts.Requests import GetProductsFilteredOnUserRequest, GetProductsTopSelledRequest
from api.serializer import ProdottoSerializer
from magazzino.models import Prodotto, Deposito, Vendita, Appartiene


class ProdottoCreate(generics.CreateAPIView):
    queryset = Prodotto.objects.all()
    serializer_class = ProdottoSerializer


class ProdottoUpdate(generics.UpdateAPIView):
    queryset = Prodotto.objects.all()
    serializer_class = ProdottoSerializer


class ProdottoDestroy(generics.DestroyAPIView):
    queryset = Prodotto.objects.all()
    serializer_class = ProdottoSerializer


class ProdottiFilteredGet(GenericAPIView):
    serializer_class = GetProductsFilteredOnUserRequest

    def post(self, request, format=None):
        serialized_data = GetProductsFilteredOnUserRequest(data=request.data)
        if not serialized_data.is_valid():
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, errors=serialized_data.errors)

        negozio_id = serialized_data.validated_data['NegozioId']

        # -1 => All
        if negozio_id == -1:
            prodotti_in_negozio = Deposito.objects.all().values()
        else:
            prodotti_in_negozio = Deposito.objects.filter(Negozio__id=negozio_id)

        prodotti_in_negozio = prodotti_in_negozio.values(NomeProdotto=F('Prodotto__Nome'),
                                                         QuantitaDepositata=F('Quantita'),
                                                         NomeNegozio=F('Negozio__Nome'), ProdottoId=F('Prodotto__id'),
                                                         NegozioId=F('Negozio__id'))
        return Response(prodotti_in_negozio)

