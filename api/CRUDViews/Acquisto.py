from rest_framework import generics

from api.serializer import AcquistoSerializer
from magazzino.models import Acquisto


class AcquistoCreate(generics.CreateAPIView):
    queryset = Acquisto.objects.all()
    serializer_class = AcquistoSerializer


class AcquistoUpdate(generics.UpdateAPIView):
    queryset = Acquisto.objects.all()
    serializer_class = AcquistoSerializer


class AcquistoDestroy(generics.DestroyAPIView):
    queryset = Acquisto.objects.all()
    serializer_class = AcquistoSerializer
