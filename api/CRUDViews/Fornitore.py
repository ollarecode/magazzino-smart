from rest_framework import generics

from api.serializer import FornitoreSerializer
from magazzino.models import Fornitore


class FornitoreCreate(generics.CreateAPIView):
    queryset = Fornitore.objects.all()
    serializer_class = FornitoreSerializer


class FornitoreUpdate(generics.UpdateAPIView):
    queryset = Fornitore.objects.all()
    serializer_class = FornitoreSerializer


class FornitoreDestroy(generics.DestroyAPIView):
    queryset = Fornitore.objects.all()
    serializer_class = FornitoreSerializer
