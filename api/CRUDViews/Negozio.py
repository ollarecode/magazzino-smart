from django.db.models import Sum
from rest_framework import generics, status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from api.Contracts.Requests import GetProductsTopStoredRequest
from api.serializer import NegozioSerializer
from magazzino.models import Negozio, Deposito, Appartiene


class NegozioCreate(generics.CreateAPIView):
    queryset = Negozio.objects.all()
    serializer_class = NegozioSerializer


class NegozioUpdate(generics.UpdateAPIView):
    queryset = Negozio.objects.all()
    serializer_class = NegozioSerializer


class NegozioDestroy(generics.DestroyAPIView):
    queryset = Negozio.objects.all()
    serializer_class = NegozioSerializer


class ProdottiInNegozio(GenericAPIView):
    serializer_class = GetProductsTopStoredRequest

    def post(self, request):
        serialized_data = GetProductsTopStoredRequest(data=request.data)
        if not serialized_data.is_valid():
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, errors=serialized_data.errors)

        negozio_id = serialized_data.validated_data['NegozioId']
        filters = serialized_data.validated_data['Filters']

        prodotti_depositati = Deposito.objects.filter(Negozio__id=negozio_id)

        if any(filters):
            product_ids = prodotti_depositati.values('Prodotto__id')
            product_ids_filtered = Appartiene.objects.filter(Prodotto__id__in=product_ids, Categoria__Nome__in=filters, Negozio__id=negozio_id).values('Prodotto__id')

            prodotti_depositati = prodotti_depositati.filter(Prodotto__id__in=product_ids_filtered)

        prodotti_depositati = prodotti_depositati.values('Prodotto__Nome').annotate(QuantitaDepositata=Sum('Quantita'))

        return Response(prodotti_depositati.values('Prodotto__Nome', 'QuantitaDepositata', 'Descrizione'))
