from django.db.models import Count, F, Sum
from rest_framework import generics, status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from api.Contracts.Requests import GetProductsTopSelledRequest
from api.serializer import VenditaSerializer
from magazzino.models import Vendita, Appartiene


class VenditaCreate(generics.CreateAPIView):
    queryset = Vendita.objects.all()
    serializer_class = VenditaSerializer


class VenditaUpdate(generics.UpdateAPIView):
    queryset = Vendita.objects.all()
    serializer_class = VenditaSerializer


class VenditaDestroy(generics.DestroyAPIView):
    queryset = Vendita.objects.all()
    serializer_class = VenditaSerializer


class TopSelledProdottiByFilter(GenericAPIView):
    serializer_class = GetProductsTopSelledRequest

    def post(self, request):
        serialized_data = GetProductsTopSelledRequest(data=request.data)
        if not serialized_data.is_valid():
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, errors=serialized_data.errors)

        negozio_id = serialized_data.validated_data['NegozioId']
        filters = serialized_data.validated_data['Filters']

        vendite_negozio = Vendita.objects.filter(Negozio__id=negozio_id)

        if any(filters):
            product_ids = vendite_negozio.values('Prodotto__id')
            product_ids_filtered = Appartiene.objects.filter(Prodotto__id__in=product_ids, Categoria__Nome__in=filters, Negozio__id=negozio_id).values('Prodotto__id')

            vendite_negozio = vendite_negozio.filter(Prodotto__id__in=product_ids_filtered)

        vendite_negozio = vendite_negozio.values('Prodotto__Nome').annotate(QuantitaVenduta=Sum('Quantita'))

        return Response(vendite_negozio.values('Prodotto__Nome', 'QuantitaVenduta'))
