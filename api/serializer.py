from rest_framework import serializers

from magazzino.models import Negozio, Prodotto, Deposito, Acquisto, Vendita, Appartiene, Categoria, Fornitore


class NegozioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Negozio
        fields = '__all__'


class ProdottoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Prodotto
        fields = '__all__'


class CategoriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Categoria
        fields = '__all__'


class FornitoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fornitore
        fields = '__all__'


class AcquistoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Acquisto
        fields = '__all__'


class VenditaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vendita
        fields = '__all__'


