
from rest_framework import serializers


class GetProductsFilteredOnUserRequest(serializers.Serializer):
    NegozioId = serializers.IntegerField()


class GetProductsTopSelledRequest(serializers.Serializer):
    NegozioId = serializers.IntegerField()
    Filters = serializers.ListField()


class GetProductsTopStoredRequest(serializers.Serializer):
    NegozioId = serializers.IntegerField()
    Filters = serializers.ListField()