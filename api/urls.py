from django.urls import path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from api.CRUDViews.Acquisto import AcquistoCreate, AcquistoUpdate, AcquistoDestroy
from api.CRUDViews.Categoria import CategoriaDestroy, CategoriaUpdate, CategoriaCreate, CategoriaList
from api.CRUDViews.Fornitore import FornitoreDestroy, FornitoreUpdate, FornitoreCreate
from api.CRUDViews.Negozio import NegozioCreate, NegozioUpdate, NegozioDestroy, ProdottiInNegozio
from api.CRUDViews.Prodotto import ProdottoCreate, ProdottoUpdate, ProdottoDestroy, ProdottiFilteredGet
from api.CRUDViews.Vendita import VenditaCreate, VenditaUpdate, VenditaDestroy, TopSelledProdottiByFilter

app_name = 'api'

schema_view = get_schema_view(
   openapi.Info(
      title="CRUD API",
      default_version='v1',
      description="CRUD METHODS",
   ),
   public=True,
   permission_classes=[permissions.AllowAny],
)

urlpatterns = [
    path(r'negozio/create', NegozioCreate.as_view(), name="acquisto-create"),
    path(r'negozio/update/<int:pk>', NegozioUpdate.as_view(), name="negozio-update"),
    path(r'negozio/delete/<int:pk>', NegozioDestroy.as_view(), name="negozio-delete"),
    path(r'negozio/topsupplied', ProdottiInNegozio.as_view(), name="negozio-get-topsupplied"),


    path(r'prodotto/create', ProdottoCreate.as_view(), name="prodotto-create"),
    path(r'prodotto/update/<int:pk>', ProdottoUpdate.as_view(), name="prodotto-update"),
    path(r'prodotto/delete/<int:pk>', ProdottoDestroy.as_view(), name="prodotto-delete"),
    path(r'prodotto/get', ProdottiFilteredGet.as_view(), name='prodotto_get_by_negozio_di'),

    path(r'categoria/create', CategoriaCreate.as_view(), name="acquisto-create"),
    path(r'categoria/update/<int:pk>', CategoriaUpdate.as_view(), name="categoria-update"),
    path(r'categoria/delete/<int:pk>', CategoriaDestroy.as_view(), name="categoria-delete"),
    path(r'categoria/list', CategoriaList.as_view(), name="categoria-list"),

    path(r'fornitore/create', FornitoreCreate.as_view(), name="acquisto-create"),
    path(r'fornitore/update/<int:pk>', FornitoreUpdate.as_view(), name="fornitore-update"),
    path(r'fornitore/delete/<int:pk>', FornitoreDestroy.as_view(), name="fornitore-delete"),

    path(r'vendita/create', VenditaCreate.as_view(), name="vendita-create"),
    path(r'vendita/update/<int:pk>', VenditaUpdate.as_view(), name="vendita-update"),
    path(r'vendita/delete/<int:pk>', VenditaDestroy.as_view(), name="vendita-delete"),
    path(r'vendita/topselled', TopSelledProdottiByFilter.as_view(), name='topselled_products_by_filter'),

    path(r'acquisto/create', AcquistoCreate.as_view(), name="acquisto-create"),
    path(r'acquisto/update/<int:pk>', AcquistoUpdate.as_view(), name="acquisto-update"),
    path(r'acquisto/delete/<int:pk>', AcquistoDestroy.as_view(), name="acquisto-delete"),

    path(r'swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui')
]
